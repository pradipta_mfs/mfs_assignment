import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductForm from './Components/ProductForm';
import TableData from './Components/TableData';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      productName: '',
      category: '',
      description: '',
      price: 0,
      list: [],
      editFlag: -1,
      error: {
        productName: '',
        category: '',
        price: ''
      }
    }
  }

  editHandler(index) {
    this.setState({
      productName: this.state.list[index].productName,
      category: this.state.list[index].category,
      description: this.state.list[index].description,
      price: this.state.list[index].price,
      editFlag: index
    })
  }

  deleteHandler(index) {
    const list = this.state.list
    list.splice(index, 1)
    this.setState({
      list: list
    })
  }

  handleChange(event) {
    const { name, value } = event.target;
    const error = Object.assign({}, this.state.error);
    switch(name) {
      case 'productName':
      case 'category':
        error[name] = value.length === 0 ? `${name} must be a valid string` : ''
        break
      case 'price':
        error[name] = value <= 0 ? `${name} must be greater than 0` : ''
        break
      default:
        break
    }
    this.setState({
      [event.target.name] : event.target.value,
      error: error
    })
  }

  handleSubmit(event) {
    event.preventDefault()
    if(!this.state.productName || !this.state.category || this.state.price <= 0) {
      return
    }
    const list = this.state.list
    if(this.state.editFlag >= 0) {
      const { productName, category, description, price } = this.state
      list[this.state.editFlag] = { productName, category, description, price }
      this.setState({
        list: list,
        editFlag: -1
      })
      return
    }
    const { productName, category, description, price } = this.state
    list.push({ productName, category, description, price })
    this.setState({
      list: list
    })
    return
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <br/>
          <ProductForm formValue={this.state} handleChange={(e) => this.handleChange(e)} handleSubmit={(e) => this.handleSubmit(e)}/>
          <TableData list={this.state.list} editHandler={(index) => this.editHandler(index)} deleteHandler={(index) => this.deleteHandler(index)}/>
        </header>
      </div>
    );
  }
}

export default App;
