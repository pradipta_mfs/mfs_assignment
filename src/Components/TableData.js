import React from 'react'
import { Table, Button } from 'react-bootstrap';

function TableData (props) {
    return (
      props.list.length === 0 ? <h1>No Product Added</h1> : (
        <Table striped bordered hover variant="dark" size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Product Name</th>
              <th>Category</th>
              <th>Description</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {
              props.list.map((item, index) => {
                return (
                  <tr key={index.toString()}>
                    <td>{index}</td>
                    <td>{item.productName}</td>
                    <td>{item.category}</td>
                    <td>{item.description}</td>
                    <td>{item.price}$</td>
                    <td>
                      <Button variant="link" onClick={() => props.editHandler(index)}>Edit</Button>
                      <Button variant="link" onClick={() => props.deleteHandler(index)}>Delete</Button>
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </Table>
      )
    );
}

export default TableData;