import React from 'react'
import { Form, Col } from 'react-bootstrap';

function PriceComponent(props) {
  return (
    <React.Fragment>
      <Form.Label column sm="6">
        Price:
      </Form.Label>
      <Col sm="3">
        <Form.Control name={props.name} type="number" value={props.price} onChange={props.handleChange} min="0" required />
        {props.error.length > 0 && <span style={{color: "red"}}>{props.error}</span>}
      </Col>
    </React.Fragment>
  );
}

export default PriceComponent;