import React from 'react'
import { Form, Col } from 'react-bootstrap';

function NameComponent(props) {
  return (
    <React.Fragment>
      <Form.Label column sm="6">
          ProductName:
      </Form.Label>
      <Col sm="3">
        <Form.Control name={props.name} type="text" value={props.productName} onChange={props.handleChange} required/>
        {props.error.length > 0 && <span style={{color: "red"}}>{props.error}</span>}
      </Col>
    </React.Fragment>
  );
}

export default NameComponent;