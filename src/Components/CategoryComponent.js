import React from 'react'
import { Form, Col } from 'react-bootstrap';

function CategoryComponent(props) {
  return (
    <React.Fragment>
      <Form.Label column sm="6">
        Category:
      </Form.Label>
      <Col sm="3">
        <Form.Control as="select" name={props.name} value={props.category || ''} onChange={props.handleChange}>
          <option value=""></option>
          <option value="Sedan">Sedan</option>
          <option value="SUV">SUV</option>
          <option value="MUV">MUV</option>
        </Form.Control>
        {props.error.length > 0 && <span style={{color: "red"}}>{props.error}</span>}
      </Col>
    </React.Fragment>
  );
}

export default CategoryComponent;