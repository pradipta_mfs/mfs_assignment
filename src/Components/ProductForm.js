import React from 'react'
import { Button, Form, Row, Col } from 'react-bootstrap';
import NameComponent from './NameComponent'
import CategoryComponent from './CategoryComponent'
import DescriptionComponent from './DescriptionComponent'
import PriceComponent from './PriceComponent'

function ProductForm (props) {
    return (
      <Form onSubmit={props.handleSubmit}>
        <Form.Group as={Row}>
          <NameComponent name="productName" productName={props.formValue.productName} handleChange={props.handleChange} error={props.formValue.error.productName}/>
          <br/><br/>
          <CategoryComponent name="category" category={props.formValue.category} handleChange={props.handleChange} error={props.formValue.error.category}/>
          <br/><br/>
          <DescriptionComponent name="description" description={props.formValue.description} handleChange={props.handleChange}/>
          <br/><br/><br/>
          <PriceComponent name="price" price={props.formValue.price} handleChange={props.handleChange} error={props.formValue.error.price}/>
          <Col sm="6">
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Col>
        </Form.Group>
      </Form>
    );
}

export default ProductForm;