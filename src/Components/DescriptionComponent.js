import React from 'react'
import { Form, Col } from 'react-bootstrap';

function DescriptionComponent(props) {
  return (
    <React.Fragment>
      <Form.Label column sm="6">
        Description:
      </Form.Label>
      <Col sm="3">
        <Form.Control as="textarea" name={props.name} value={props.description} onChange={props.handleChange}/>
      </Col>
    </React.Fragment>
  );
}

export default DescriptionComponent;